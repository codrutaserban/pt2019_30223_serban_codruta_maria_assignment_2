# Sistem de procesare a cozilor #

Problema presupune implementarea unui sistem de simulare a evoluției cozilor unui server. 

Problema se poate rezolva prin construcția fiecărei cozi cu un obiect Coada care contine 
un ArrayList de obiecte Client care sa memoreze și simuleze evoluția unei singure cozi. 
Toate cozile trebuie sa lucreze in paralel, prin urmare solutia de implementare a acestora 
este folosirea threadurilor. Clasa Coada  implementează interfața Runnable. Simularea sosirii 
clienților trebuie făcută în timp real. Aceasta se face cu un obiect Simulare care 
implementează interfața Runnable și care generează un timp aleator de sosire a clienților 
într-un interval dat prin metoda adaugareClient  generare, un timp aleator de servire într-un
interval dat și distribuie clientul generat în coada cu timpul cel mai redus de așteptare prin metoda .

Datele de intrare sunt timpul de inceput si sfarsit al simularii, timpul minim și timpul maxim în care
ajung clienții unul față de celălalt, timpul de servire minim și maxim și numărul de cozi si un inceput
si sfarsit al unui anumit interval, toate în format int.

Programul se deschide cu interfața grafica. Se introduc parametrii doriți, se selectează numărul de cozi
și se apasă butonul „Start simulare”. La apăsarea butonului se citesc parametrii introduși și incepe simularea.
Pe parcurs sunt generate datele pentru ora de varf a simularii, cea a unui interval specificat, pentru timpul
mediu de asteptare pe simulare si pe un anumit interval de timp dat de utilizator.