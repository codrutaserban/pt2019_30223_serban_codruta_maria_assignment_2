package View;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View extends JFrame  {
	private	JLabel sosire1 ;
	private	JLabel sosire2 ;
	private JTextField tSosire1 ;
	private JTextField tSosire2 ;
	private JLabel servire1 ;
	private JLabel servire2 ;
	private JTextField tServire1 ;
	private JTextField tServire2 ;
	private JComboBox cb ;
	private JButton bt; 
	private	JLabel start1 ;
	private	JLabel start2 ;
	private JTextField tStart1 ;
	private JTextField tStart2 ;
	private JLabel interval1 ;
	private JLabel interval2 ;
	private JTextField tInterval1 ;
	private JTextField tInterval2 ;
	private JLabel secunda ;
	private JTextField tSecunda ;
	private JLabel oraVarf ;
	private JTextField tOraVarf ;
	private JLabel oraVarfInt ;
	private JTextField tOraVarfInt ;
	private JTextArea tCoada1 ;
	private JTextArea tCoada2 ;
	private JTextArea tCoada3 ;
	private JTextArea tCoada4 ;
	private JTextArea tCoada5 ;
	private ArrayList<JTextArea> cozi;
	private JLabel timpServire ;
	private JTextField tTimpServire ;
	private JLabel timpServireInt ;
	private JTextField tTimpServireInt ;
	private JLabel timpAsteptare ;
	private JTextField tTimpAsteptare ;
	private JLabel timpAsteptareInt ;
	private JTextField tTimpAsteptareInt ;
	private JFrame frame;
	
	public View() {
	 JLabel coada1 ;
	 JLabel coada2 ;
	 JLabel coada3 ;
	 JLabel coada4 ;
	 JLabel coada5 ;
		frame = new JFrame("Simulare cozi");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1000, 1000);
		
		
		
		sosire1 = new JLabel("Sosire min:", JLabel.RIGHT);
		sosire1.setFont(new Font("Arial",Font.PLAIN,20));
		sosire2 = new JLabel("max:", JLabel.RIGHT);
		sosire2.setFont(new Font("Arial",Font.PLAIN,20));
		servire2 = new JLabel("max:", JLabel.RIGHT);
		servire1 = new JLabel("Servire min:", JLabel.RIGHT);
		servire1.setFont(new Font("Arial",Font.PLAIN,20));
		servire2.setFont(new Font("Arial",Font.PLAIN,20));
		tSosire1 = new JTextField("",2);
		tSosire1.setFont(new Font("Arial",Font.PLAIN,20));
		tSosire2 = new JTextField("",2);
		tSosire2.setFont(new Font("Arial",Font.PLAIN,20));
		tServire1 = new JTextField("",2);
		tServire1.setFont(new Font("Arial",Font.PLAIN,20));
		tServire2 = new JTextField("",2);
		tServire2.setFont(new Font("Arial",Font.PLAIN,20));
		cb = new JComboBox(new String[]{"1","2", "3", "4", "5"});
		bt = new JButton("Start simulare");
		bt.setFont(new Font("Arial",Font.PLAIN,25));
		cb.setFont(new Font("Arial",Font.PLAIN,20));
		sosire1.setFont(new Font("Arial",Font.PLAIN,20));
		sosire2.setFont(new Font("Arial",Font.PLAIN,20));
		servire1.setFont(new Font("Arial",Font.PLAIN,20));
		servire2.setFont(new Font("Arial",Font.PLAIN,20));
		
		
		coada1 = new JLabel("Coada 1", JLabel.RIGHT);
		coada2 = new JLabel("Coada 2", JLabel.RIGHT);
		coada3 = new JLabel("Coada 3", JLabel.RIGHT);
		coada4 = new JLabel("Coada 4", JLabel.RIGHT);
		coada5 = new JLabel("Coada 5", JLabel.RIGHT);
		coada1.setFont(new Font("Arial",Font.PLAIN,20));
		coada2.setFont(new Font("Arial",Font.PLAIN,20));
		coada3.setFont(new Font("Arial",Font.PLAIN,20));
		coada4.setFont(new Font("Arial",Font.PLAIN,20));
		coada5.setFont(new Font("Arial",Font.PLAIN,20));
		
		
		start1 = new JLabel("Start simulare:", JLabel.RIGHT);
		start1.setFont(new Font("Arial",Font.PLAIN,20));
		start2 = new JLabel("Stop simulare:", JLabel.RIGHT);
		start2.setFont(new Font("Arial",Font.PLAIN,20));
		interval2 = new JLabel("Interval max:", JLabel.RIGHT);
		interval2.setFont(new Font("Arial",Font.PLAIN,20));
		interval1 = new JLabel("Interval min:", JLabel.RIGHT);
		interval1.setFont(new Font("Arial",Font.PLAIN,20));
		tStart1 = new JTextField("",2);
		tStart1.setFont(new Font("Arial",Font.PLAIN,20));
		tStart2 = new JTextField("",2);
		tStart2.setFont(new Font("Arial",Font.PLAIN,20));
		tInterval1 = new JTextField("",2);
		tInterval1.setFont(new Font("Arial",Font.PLAIN,20));
		tInterval2 = new JTextField("",2);
		tInterval2.setFont(new Font("Arial",Font.PLAIN,20));
		secunda = new JLabel("Secunda", JLabel.RIGHT);
		secunda.setFont(new Font("Arial",Font.PLAIN,20));
		tSecunda = new JTextField("",2);
		tSecunda.setFont(new Font("Arial",Font.PLAIN,20));
		secunda.setFont(new Font("Arial",Font.PLAIN,20));
		secunda.setFont(new Font("Arial",Font.PLAIN,20));
		tSecunda.setFont(new Font("Arial",Font.PLAIN,20));
		tSecunda.setFont(new Font("Arial",Font.PLAIN,20));
		
		
		tCoada1=new JTextArea(15,5);
		tCoada1.setEditable(false);
		tCoada2=new JTextArea(15,5);
		tCoada2.setEditable(false);
		tCoada3=new JTextArea(15,5);
		tCoada3.setEditable(false);
		tCoada4=new JTextArea(15,5);
		tCoada4.setEditable(false);
		tCoada5=new JTextArea(15,5);
		tCoada5.setEditable(false);
		
		oraVarf = new JLabel("Ora de varf:", JLabel.RIGHT);
		oraVarf.setFont(new Font("Arial",Font.PLAIN,20));
		tOraVarf = new JTextField("",2);
		tOraVarf.setFont(new Font("Arial",Font.PLAIN,20));
		oraVarfInt = new JLabel("Ora de varf interval:", JLabel.RIGHT);
		oraVarfInt.setFont(new Font("Arial",Font.PLAIN,20));
		tOraVarfInt = new JTextField("",2);
		tOraVarfInt.setFont(new Font("Arial",Font.PLAIN,20));
		
		timpServire = new JLabel("Timp mediu servire:", JLabel.RIGHT);
		timpServire.setFont(new Font("Arial",Font.PLAIN,20));
		tTimpServire = new JTextField("",2);
		tTimpServire.setFont(new Font("Arial",Font.PLAIN,20));
		timpServireInt = new JLabel("Timp mediu servire interval:", JLabel.RIGHT);
		timpServireInt.setFont(new Font("Arial",Font.PLAIN,20));
		tTimpServireInt = new JTextField("",2);
		tTimpServireInt.setFont(new Font("Arial",Font.PLAIN,20));
		
		timpAsteptare = new JLabel("Timp mediu asteptare:", JLabel.RIGHT);
		tTimpAsteptare = new JTextField("",2);
		tTimpAsteptare.setFont(new Font("Arial",Font.PLAIN,20));
		timpAsteptareInt = new JLabel("Timp mediu asteptare interval:", JLabel.RIGHT);
		tTimpAsteptareInt = new JTextField("",2);
		tTimpAsteptareInt.setFont(new Font("Arial",Font.PLAIN,20));
		
		
		JPanel panel= new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
	    JPanel pol1 = new JPanel();
	    pol1.setLayout(new BoxLayout(pol1, BoxLayout.X_AXIS));
	    pol1.add( Box.createRigidArea(new Dimension(300,0)) );
	    pol1.add(secunda);
	    tSecunda.setEnabled(false);
	    pol1.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol1.add(tSecunda);
	    pol1.add( Box.createRigidArea(new Dimension(350,0)) );
	    
	    JPanel pol2 = new JPanel();
	    pol2.setLayout(new BoxLayout(pol2, BoxLayout.X_AXIS));
	    pol2.add( Box.createRigidArea(new Dimension(60,0)) );
	    pol2.add(coada1);
	    pol2.add( Box.createRigidArea(new Dimension(110,0)) );
	    pol2.add(coada2);
	    pol2.add( Box.createRigidArea(new Dimension(110,0)) );
	    pol2.add(coada3);
	    pol2.add( Box.createRigidArea(new Dimension(110,0)) );
	    pol2.add(coada4);
	    pol2.add( Box.createRigidArea(new Dimension(110,0)) );
	    pol2.add(coada5);
	    pol2.add( Box.createRigidArea(new Dimension(70,0)) );
	    
	    JPanel pol3 = new JPanel();
	    pol3.setLayout(new BoxLayout(pol3, BoxLayout.X_AXIS));
	    pol3.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol3.add(tCoada1);
	    tCoada1.setFont(new Font("Arial",Font.PLAIN,20));
	    pol3.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol3.add(tCoada2);
	    tCoada2.setFont(new Font("Arial",Font.PLAIN,20));
	    pol3.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol3.add(tCoada3);
	    tCoada3.setFont(new Font("Arial",Font.PLAIN,20));
	    pol3.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol3.add(tCoada4);
	    tCoada4.setFont(new Font("Arial",Font.PLAIN,20));
	    pol3.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol3.add(tCoada5);
	    tCoada5.setFont(new Font("Arial",Font.PLAIN,20));
	    pol3.add( Box.createRigidArea(new Dimension(30,0)) );
	    
	    cozi=new ArrayList<JTextArea>(); 
	    cozi.add(tCoada1);
	    cozi.add(tCoada2);
	    cozi.add(tCoada3);
	    cozi.add(tCoada4);
	    cozi.add(tCoada5);
	    
	    JPanel pol4 = new JPanel();
	    pol4.setLayout(new BoxLayout(pol4, BoxLayout.X_AXIS));
	    pol4.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol4.add(oraVarf);
	    pol4.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol4.add(tOraVarf);
	    pol4.add( Box.createRigidArea(new Dimension(70,0)) );
	    pol4.add(oraVarfInt);
	    pol4.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol4.add(tOraVarfInt);
	    pol4.add( Box.createRigidArea(new Dimension(30,0)) );
	    
	    JPanel pol5 = new JPanel();
	    pol5.setLayout(new BoxLayout(pol5, BoxLayout.X_AXIS));
	    pol5.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol5.add(timpServire);
	    pol5.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol5.add(tTimpServire);
	    pol5.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol5.add(timpServireInt);
	    pol5.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol5.add(tTimpServireInt);
	    pol5.add( Box.createRigidArea(new Dimension(30,0)) );
	    
	    JPanel pol6 = new JPanel();
	    pol6.setLayout(new BoxLayout(pol6, BoxLayout.X_AXIS));
	    pol6.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol6.add(timpAsteptare);
	    pol6.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol6.add(tTimpAsteptare);
	    pol6.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol6.add(timpAsteptareInt);
	    pol6.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol6.add(tTimpAsteptareInt);
	    pol6.add( Box.createRigidArea(new Dimension(30,0)) );
	    
	    JPanel pol7 = new JPanel();
	    pol7.setLayout(new BoxLayout(pol7, BoxLayout.X_AXIS));
	    pol7.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol7.add(sosire1);
	    pol7.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol7.add(tSosire1);
	    pol7.add( Box.createRigidArea(new Dimension(20,0)) );
	    pol7.add(sosire2);
	    pol7.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol7.add(tSosire2);
	    pol7.add( Box.createRigidArea(new Dimension(60,0)) );
	    pol7.add(servire1);
	    pol7.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol7.add(tServire1);
	    pol7.add( Box.createRigidArea(new Dimension(20,0)) );
	    pol7.add(servire2);
	    pol7.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol7.add(tServire2);
	    pol7.add( Box.createRigidArea(new Dimension(30,0)) );
	    
	    JPanel pol8 = new JPanel();
	    pol8.setLayout(new BoxLayout(pol8, BoxLayout.X_AXIS));
	    pol8.add( Box.createRigidArea(new Dimension(30,0)) );
	    pol8.add(start1);
	    pol8.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol8.add(tStart1);
	    pol8.add( Box.createRigidArea(new Dimension(20,0)) );
	    pol8.add(start2);
	    pol8.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol8.add(tStart2);
	    pol8.add( Box.createRigidArea(new Dimension(60,0)) );
	    pol8.add(interval1);
	    pol8.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol8.add(tInterval1);
	    pol8.add( Box.createRigidArea(new Dimension(20,0)) );
	    pol8.add(interval2);
	    pol8.add( Box.createRigidArea(new Dimension(15,0)) );
	    pol8.add(tInterval2);
	    pol8.add( Box.createRigidArea(new Dimension(30,0)) );
	    
	    JPanel pol9 = new JPanel();
	    pol9.setLayout(new BoxLayout(pol9, BoxLayout.X_AXIS));
	    pol9.add( Box.createRigidArea(new Dimension(50,0)) );
	    pol9.add(cb);
	    pol9.add( Box.createRigidArea(new Dimension(200,0)) );
	    pol9.add(bt);
	    pol9.add( Box.createRigidArea(new Dimension(450,0)) );
	    
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(pol1);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(pol2);
	    panel.add( Box.createRigidArea(new Dimension(0,15)) );
	    panel.add(pol3);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(pol4);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(pol5);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	   // panel.add(pol6);
	   // panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(pol7);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(pol8);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    panel.add(pol9);
	    panel.add( Box.createRigidArea(new Dimension(0,25)) );
	    
	    
	    
	    frame.add(panel);
		frame.setVisible(true);
	}
	public void addButonListener(ActionListener a) {
		bt.addActionListener(a);
	}
	
	public int getNrCozi() {
		return Integer.parseInt( (String)cb.getSelectedItem() ); 
	}
	
	public void setOraVarf(int count)
	{
		tOraVarf.setText(Integer.toString(count));
	}
	public void setOraVarfInt(int count)
	{
		tOraVarfInt.setText(Integer.toString(count));
	}
	
	public void setCount(int count)
	{
		tSecunda.setText(Integer.toString(count));
	}
	
	public void setServire(double count)
	{
		 tTimpServire.setText(Double.toString(count));
	}
	
	public void setServireInt(double count)
	{
		 tTimpServireInt.setText(Double.toString(count));
	}
	
	public void setAsteptare(double count)
	{
		 tTimpAsteptare.setText(Double.toString(count));
	}
	
	public void setAsteptareInt(double count)
	{
		 tTimpAsteptareInt.setText(Double.toString(count));
	}
	
	
	public int getSosire1() {
		return Integer.parseInt(tSosire1.getText());
	}
	public int getSosire2() {
		return Integer.parseInt(tSosire2.getText());
	}
	public int getServire1() {
		return Integer.parseInt(tServire1.getText());
	}
	public int getServire2() {
		return Integer.parseInt(tServire2.getText());
	}
	public int getStart1() {
		return Integer.parseInt(tStart1.getText());
	}
	public int getStart2() {
		return Integer.parseInt(tStart2.getText());
	}
	public int getInterval1() {
		return Integer.parseInt(tInterval1.getText());
	}
	public int getInterval2() {
		return Integer.parseInt(tInterval2.getText());
	}
	public JFrame getFrame() {
		return frame;
	}
	public void setCozi(ArrayList<String> text) {
		for( int i=0;i<text.size();i++ )
		{
			cozi.get(i).setText(text.get(i));
		}
	}
}
