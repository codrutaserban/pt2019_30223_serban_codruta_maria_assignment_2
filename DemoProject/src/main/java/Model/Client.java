package Model;

import java.util.Random;

public class Client implements Comparable<Client> {
	private int timpServire;
	private int timpSosire;
	private int timpPlecare;
	private String nume;
	
	public Client(int timpServire, int timpSosire,String nume)
	{
		this.timpServire=timpServire;
		this.timpSosire=timpSosire;
		this.nume=nume;
	}

	public int compareTo(Client c1) {
		if(this.timpSosire==c1.timpSosire)return 0;
		else if(this.timpSosire>c1.timpSosire) return 1;
		else return -1;
	}
	
	public int getTimpServire() {
		return timpServire;
	}

	public void setTimpServire(int timpServire) {
		this.timpServire = timpServire;
	}

	public int getTimpSosire() {
		return timpSosire;
	}

	public void setTimpSosire(int timpSosire) {
		this.timpSosire = timpSosire;
	}

	public int getTimpPlecare() {
		return timpPlecare;
	}

	public void setTimpPlecare(int timpPlecare) {
		this.timpPlecare = timpPlecare;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}
	
}
