package Model;

import java.util.ArrayList;

public class Programare {
	private ArrayList<Coada> l;
	private double timpMediuAsteptare;
	private int timpAsteptare;
	private Thread t;
	
	public Programare(int nr)
	{
		timpAsteptare=0;
		timpMediuAsteptare=0;
		l=new ArrayList<Coada>();
		for(int i=0;i<nr;i++)
		{
			l.add(new Coada());
		     t= new Thread (l.get(i));
			t.start();	
		}
	}
	
	public  int adaugareClient(Client c)
	{   int min=0;
		
		double minTimp=l.get(0).timpAsteptareMediu();
		for( Coada i:l){
			if(i.getQ().isEmpty()) {
				
				i.adaugare(c);
	
				return l.indexOf(i);
			}
			else if(i.timpAsteptareMediu()<minTimp){
				minTimp= i.timpAsteptareMediu();
				min=l.indexOf(i);
				 try 
			        { 
			            Thread.sleep(500); 
			        }  
			        catch (Exception e)  
			        { 
			            System.out.println("Thread interrupted."); 
			        } 
			//	Client list[]= new Client[l.get(min).getQ().size()];
			//	Client aux= list[l.get(min).getQ().size()-1];
			//	timpAsteptare+=aux.getTimpPlecare()-c.getTimpSosire();
			//	c.setTimpPlecare(aux.getTimpPlecare()+c.getTimpServire());
			//	timpMediuAsteptare=timpAsteptare/Double.parseDouble(c.getNume());
			}
		}		
		
		l.get(min).adaugare(c);	
		System.out.println("Adaug in coada:"+min);
		return min;
	}
	
	public int numarClienti() {
		int nr=0;
		for(Coada q:l)
		{
			nr=nr+q.marime();
		}
		return nr;
	}

	public ArrayList<Coada> getL() {
		return l;
	}


	public void setL(ArrayList<Coada> l) {
		this.l = l;
	}


	public double getTimpMediuAsteptare() {
		return timpMediuAsteptare;
	}


	public void setTimpMediuAsteptare(double timpMediuAsteptare) {
		this.timpMediuAsteptare = timpMediuAsteptare;
	}
	
	

}
