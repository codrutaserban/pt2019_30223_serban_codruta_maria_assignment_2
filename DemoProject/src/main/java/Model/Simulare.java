package Model;

import java.util.*;

import javax.swing.JFrame;

import View.View;

public class Simulare implements Runnable {
private int nrCozi;	
private int timpSimulareStart;
private int timpSimulareStop;
private int minTimpSosire;
private int maxTimpSosire;
private int minTimpServire;
private int maxTimpServire;
private double timpMediuServire;
private double timpMediuAsteptare;
private double timpMediuAsteptareInt;
private int oraVarf;
private int oraVarfInt;//peak hour pe un anumit interval
private int minInt;//capetele intervalului specificat
private int maxInt;
private double timpMediuServireInt;
private int count;
private Programare p;	
private View v;
	public Simulare(int nr,int timpStart,int timpStop,int minTSos,int maxTSos,int minTSer,int maxTSer,View v,int minInt1, int maxInt1){
		timpMediuServire=0; 
		 timpMediuServireInt= 0;
		 minTimpSosire=minTSos;
		 maxTimpSosire=maxTSos;
		 minTimpServire=minTSer;
		 maxTimpServire=maxTSer;
		 nrCozi=nr;
		 count=timpSimulareStart;
		 this.v=v;
		 p= new Programare(nr);
		 oraVarf=0;
		 oraVarfInt=0;
		 timpSimulareStop=timpStop;
		 timpSimulareStart=timpStart;
		 minInt=maxInt1;
		 minInt=minInt1;
		
	}


	 public void run()
	 {   int index=0,nrInv=0;        //index indexul clientului dintre toti cei generati random nrInv numarul clientilor care au fost adaugati in intervalul specificat  
	   int max=0,maxInt1=0,coadaCurenta;     //max=nr maxim de clienti la momentul ora
	   double sum=0;
	   ArrayList<String> sir;
	   ArrayList<Client> clienti= new ArrayList<Client> ();
	   clienti=generare(nrCozi,minTimpSosire, maxTimpSosire, minTimpServire,maxTimpServire);
		 while(timpSimulareStop>=count && index<clienti.size()) {
			 if(count==clienti.get(index).getTimpSosire())	{
					coadaCurenta=p.adaugareClient(clienti.get(index));
					System.out.println("Clientul "+clienti.get(index).getNume()+" a sosit. Coada: "+coadaCurenta);
					sum=clienti.get(index++).getTimpServire();
				}	
			 if(max<p.numarClienti()){
				 max=p.numarClienti();
				 oraVarf=count;
			 }
			 if(count>=minInt && count<=maxInt) {
				if(maxInt1<p.numarClienti())
				{ 
				//if(oraVar)	
				maxInt1=p.numarClienti();
				 oraVarfInt= count;}
			    nrInv++;
			    timpMediuServireInt +=clienti.get(index).getTimpServire();
			 }
			 timpMediuServire=(double)sum/index; 
			 timpMediuServireInt/= nrInv;
			 sir=this.listare(p.getL());
			 v.setCozi(sir);
			 v.setCount(count);
			 v.setServire(timpMediuServire);
			if(count>=minInt && count<= maxInt) {
				v.setServireInt(timpMediuServireInt);
				v.setAsteptareInt(timpMediuAsteptareInt);
				v.setOraVarfInt(oraVarfInt);
			}
			 v.setAsteptare(timpMediuAsteptare);
			 v.setOraVarf(oraVarf);
			 try {
					Thread.sleep(1000);	
					count++;
				}
				catch (InterruptedException e){
					System.out.println("InterruptedException occur"); }
		}
		 
	 }
	 
	 public ArrayList<Client> generare(int nrCozi,int minTimpSosire, int maxTimpSosire, int minTimpServire, int maxTimpServire) {
		 ArrayList<Client> a= new ArrayList<Client>();
		 Random r=new Random();
		 a.add(new Client(r.nextInt(1+maxTimpServire-minTimpServire) +minTimpServire,1,"1"));
		 a.get(0).setTimpPlecare(a.get(0).getTimpSosire()+a.get(0).getTimpServire());
		 for(int i=1;i<nrCozi*20;i++)
		 {
			 a.add(new Client(r.nextInt(1+maxTimpServire-minTimpServire) +minTimpServire,a.get(i-1).getTimpSosire()+r.nextInt(1+maxTimpSosire-minTimpSosire) +minTimpSosire,Integer.toString(i+1)));
		 }
		 Collections.sort(a);
		 return a;
	 }
	 
	 
	 
	 public ArrayList<String> listare (ArrayList<Coada> l)
	 {
		 ArrayList<String> sir= new ArrayList<String>();
		 String s;
		 for(Coada c:l)
		 {
			 s= new String();
			 for(Client aux:c.getQ())
			 {
				 s=s+"Client "+aux.getNume()+"\n";
			 }
			 sir.add(s);
		 }
		 return sir;
	 }

		public Programare getP() {
			return p;
		}


		public void setP(Programare p) {
			this.p = p;
		}


		public int getCount() {
			return count;
		}

		
	 
}
