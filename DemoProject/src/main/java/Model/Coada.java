package Model;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JFrame;


public class Coada implements Runnable {
	private LinkedBlockingQueue<Client> q;
	private double timpMediuAsteptare;
	private int timpServireTotal;
	
	Coada ()
	{
		q = new LinkedBlockingQueue<>();
		timpServireTotal=0;
	}
	
	public void run()
	{
		Client aux;
		while(true) {
		try {
			if(!q.isEmpty()) {
				aux=q.peek();
				timpServireTotal= timpServireTotal +aux.getTimpServire();
				Thread.sleep((aux.getTimpServire()+1)*1000);
				aux=q.remove();
		System.out.println("Clientul "+aux.getNume()+" a plecat.");
			}
		}
		catch (InterruptedException e){
			System.out.println("InterruptedException occur"); }
		
		}
	}
	
	public void adaugare(Client c)
	{
		q.add(c);
	}
	
	public Client eliminare()
	{
	  return q.remove();
	}
	
	public double timpAsteptareMediu()
	{
		double avg=0;
		if (q.isEmpty()){
			return 0;
		}
		for(Client i:q){
			avg=avg+i.getTimpServire();
		}
		 avg/=(double)q.size();
		 timpMediuAsteptare= avg;
		 return avg;
	}
	
	public int marime()
	{
		return q.size();
	}
	

	public LinkedBlockingQueue<Client> getQ() {
		return q;
	}

	public void setQ( LinkedBlockingQueue<Client>  q) {
		this.q = q;
	}

	public double getTimpMediuAsteptare() {
		return timpMediuAsteptare;
	}

	public void setTimpMediuAsteptare(double timpMediuAsteptare) {
		this.timpMediuAsteptare = timpMediuAsteptare;
	}
	
}


