package Coontroller;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Model.Simulare;
import View.View;

public class Controller {
	private View v;
	private Simulare s;
	private int nrCozi;
	private int timpSimulareStart;
	private int timpSimulareStop;
	private int minTimpSosire;
	private int maxTimpSosire;
	private int minTimpServire;
	private int maxTimpServire;
	private int minInt;//capetele intervalului specificat
	private int maxInt;
	
	
	public Controller(View v){
		this.v=v;
		v.addButonListener(new citire());
	}
	
	private class citire implements ActionListener {
		public void actionPerformed(ActionEvent e){
			timpSimulareStart=v.getStart1();
			timpSimulareStop=v.getStart2();
			minTimpServire=v.getServire1();
			maxTimpServire=v.getServire2();
			minTimpSosire=v.getSosire1();
			maxTimpSosire=v.getSosire2();
			minInt=v.getInterval1();
			maxInt=v.getInterval2();
			nrCozi= v.getNrCozi();
			s=new Simulare(nrCozi,timpSimulareStart,timpSimulareStop,minTimpSosire,maxTimpSosire,minTimpServire,maxTimpServire,v,minInt,maxInt);
			Thread t= new Thread (s);
			t.start();
		}
	}
}
